FROM node:latest
MAINTAINER Sucipto <chip@pringstudio.com>

RUN npm install -g hexo && npm install -g firebase-tools
